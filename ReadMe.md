### Nintex Checkout Client

The product checkout view page.

This is built using my open source react [boilerplate](https://github.com/dibosh/simple-react-template)

#### Setup

- Install dependencies: `npm install`

#### Running the Project

- This is the client, so build it first: `npm run build-dev`

- Serve this using the `nintex-checkout-server`, by simply going to the server
folder and typing this from terminal: ` CLIENT_DIR=<this-folders-absolute-path> npm start`

- Head to `localhost:8282` to see the client in action


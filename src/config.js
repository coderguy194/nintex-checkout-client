import Home from './views/Home/Home.js';

const routes = [
  {
    path: '/',
    component: Home
  }
];

export default {
  routes
};

import React from 'react';
import PropTypes from 'prop-types';

class ProductListItem extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 0
    };

    this.handleDecrement = this.handleDecrement.bind(this);
    this.handleIncrement = this.handleIncrement.bind(this);
  }

  handleIncrement() {
    this.setState(({count}) => {
      count += 1;
      count = count > 20 ? 20 : count;
      this.props.onCountUpdate({
        productId: this.props.product.productId,
        count
      });
      return {count};
    });
  }

  handleDecrement() {
    this.setState(({count}) => {
      count -= 1;
      count = count < 0 ? 0 : count;
      this.props.onCountUpdate({
        productId: this.props.product.productId,
        count
      });
      return {count};
    });
  }

  componentDidMount() {
    this.setState({count: this.props.initialCount || 0});
  }

  render() {
    let {product} = this.props;
    let {count} = this.state;

    return (
      <li className="list-group-item">
        <div className="d-flex flex-row">
          <div className="mr-auto">
            <h5>{product.productName}</h5>
            <p>${product.price}</p>
          </div>
          <div className="btn-group">
            <button className="btn btn-primary" onClick={this.handleDecrement}>-</button>
            <button className="btn btn-outline-primary disabled">{count}</button>
            <button className="btn btn-primary" onClick={this.handleIncrement}>+</button>
          </div>
        </div>
      </li>
    );
  }
}

ProductListItem.propTypes = {
  product: PropTypes.object.isRequired,
  initialCount: PropTypes.number,
  onCountUpdate: PropTypes.func.isRequired
};

export default ProductListItem;

import React from 'react';
import PropTypes from 'prop-types';
import filter from 'lodash/filter';
import some from 'lodash/some';

function CartItemList({items}) {
  return (
    <ul className="list-group">
      {
        filter(items, item => item.count)
          .map(item => (
            <li className="list-group-item" key={JSON.stringify(item)}>
              <div className="d-flex flex-row">
                <div className="mr-auto">
                  <h6>{item.type.productName}</h6>
                  <p>{item.count} x ${item.type.price}</p>
                </div>
                <div className="text-center">
                  {(item.count * item.type.price).toFixed(2)}
                </div>
              </div>
            </li>
          ))
      }
    </ul>
  );
}

function Cart({cart}) {
  let totalPayableAmount = cart.totalPayableAmount;
  let productAndCounts = Object.values(cart.productCountMap);
  let shouldShowCartItemList = some(productAndCounts, 'count');

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">Your Cart</h5>
        <h6 className="card-subtitle mb-2 text-muted">Total: {totalPayableAmount.toFixed(2)}</h6>
        {totalPayableAmount === 0 && <p className="card-text">Nothing added yet</p>}
        {shouldShowCartItemList && <CartItemList items={productAndCounts}/>}
        {
          shouldShowCartItemList && <a
            href="#"
            className="btn btn-success mt-2">
            Pay {totalPayableAmount.toFixed(2)}
          </a>
        }
      </div>
    </div>
  );
}

Cart.propTypes = {
  cart: PropTypes.object.isRequired
};

export default Cart;

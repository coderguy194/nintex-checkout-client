import React from 'react';
import PropTypes from 'prop-types';

function PromoCodeInput({onPromoCodeUpdate, errorText}) {
  return (
    <div className="form-group mt-5">
      <label htmlFor="promoCode">Promo Code</label>
      <input type="text"
             className="form-control"
             id="promoCode"
             onChange={(evt) => onPromoCodeUpdate(evt.target.value)}
             placeholder="Enter promo code"/>
      {errorText && <small className="form-text text-danger">
        {errorText}
      </small>}
    </div>
  );
}

PromoCodeInput.propTypes = {
  onPromoCodeUpdate: PropTypes.func.isRequired,
  errorText: PropTypes.string.isRequired
};

export default PromoCodeInput;

import React from 'react';
import PropTypes from 'prop-types';
import httpService from '../../services/http';
import ProductListItem from '../ProductListItem/ProductListItem';

class ProductList extends React.Component {
  constructor() {
    super();
    this.state = {
      productCountMap: {}
    };

    this.onSingleProductCountUpdate = this.onSingleProductCountUpdate.bind(this);
  }

  componentDidMount() {
    httpService.get('/products', {}, {})
      .then((products) => {
        let productCountMap = {};
        products.forEach((product) => {
          productCountMap[product.productId] = {
            type: product,
            count: 0
          };
        });

        this.setState({productCountMap});
      });
  }

  onSingleProductCountUpdate(update) {
    let {productCountMap} = this.state;
    productCountMap[update.productId].count = update.count;
    this.setState(productCountMap, () => {
      this.props.onProductCountMapUpdate(this.state.productCountMap);
    });
  }

  render() {
    let listItems = Object.values(this.state.productCountMap)
      .map((productCount) =>
        <ProductListItem
          key={JSON.stringify(productCount.type)}
          product={productCount.type}
          initialCount={productCount.count}
          onCountUpdate={this.onSingleProductCountUpdate}/>
      );

    return (
      <ul className="list-group">
        {listItems}
      </ul>
    );
  }
}

ProductList.propTypes = {
  onProductCountMapUpdate: PropTypes.func.isRequired
};

export default ProductList;

import React from 'react';
import isEmpty from 'lodash/isEmpty';
import httpService from '../../services/http';
import ProductList from '../../components/ProductList/ProductList';
import Cart from '../../components/Cart/Cart';
import PromoCodeInput from '../../components/PromoCodeInput/PromoCodeInput';
import {getModifiedCartBasedOnPromotionRule, calculateTotalPayableAmount} from '../../services/promotionHelperService';

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      cart: {
        totalPayableAmount: 0,
        productCountMap: {}
      },
      cartWithoutPromo: {
        totalPayableAmount: 0,
        productCountMap: {}
      },
      currentVisiblePromoCode: '',
      promoCodeErrorText: ''
    };

    this.onProductCountMapUpdate = this.onProductCountMapUpdate.bind(this);
    this.onPromoCodeUpdate = this.onPromoCodeUpdate.bind(this);
  }

  onProductCountMapUpdate(productCountMap) {
    let totalPayableAmount = calculateTotalPayableAmount(productCountMap);
    let cart = this.state.cart;
    cart.totalPayableAmount = totalPayableAmount;
    cart.productCountMap = productCountMap;
    this.setState({cart, cartWithoutPromo: cart}, () => {
      // product cart has been changed, if any previous promo code is still in
      // input box, reapply it
      if (!isEmpty(this.state.currentVisiblePromoCode)) {
        this._processPromoCode(this.state.currentVisiblePromoCode);
      }
    });
  }

  onPromoCodeUpdate(promoCode) {
    this.setState({currentVisiblePromoCode: promoCode});
    this._processPromoCode(promoCode);
  }

  _processPromoCode(promoCode) {
    if (!promoCode.trim()) {
      this.setState({cart: this.state.cartWithoutPromo});
      this.setState({promoCodeErrorText: ''});
    } else {
      httpService.get(`/promotions/${promoCode}`, {}, {})
        .then((promotionRule) => {
          if (!isEmpty(promotionRule)) {
            try {
              let modifiedCart = getModifiedCartBasedOnPromotionRule(
                promotionRule,
                this.state.cartWithoutPromo
              );
              this.setState({cart: modifiedCart});
            } catch (error) {
              this._setPromoCodeErrorText(error.message);
            }
          } else {
            this._setPromoCodeErrorText('No promo code available like this');
          }
        });
    }
  }

  _setPromoCodeErrorText(text) {
    this.setState({promoCodeErrorText: text});
    setTimeout(() => {
      this.setState({promoCodeErrorText: ''});
    }, 3000);
  }

  render() {
    let shouldShowPromoField = this.state.cart.totalPayableAmount > 0;
    return (
      <div className="container py-4">
        <div className="jumbotron" style={{height: '92vh'}}>
          <h4>Choose Products</h4>
          <div className="row">
            <div className="col-8">
              <ProductList onProductCountMapUpdate={this.onProductCountMapUpdate}/>
              {
                shouldShowPromoField && <PromoCodeInput
                  onPromoCodeUpdate={this.onPromoCodeUpdate}
                  errorText={this.state.promoCodeErrorText}/>
              }
            </div>
            <div className="col-4">
              <Cart cart={this.state.cart}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;

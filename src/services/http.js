import axios from 'axios';

class Http {
  get(url, params, headers) {
    return this._make('get', url, headers, params);
  }

  post(url, data, headers) {
    return this._make('post', url, headers, null, data);
  }

  _make(method, url, headers, params, data, requestTransformer, responseTransformer) {
    let config = {
      baseURL: '/api',
      method,
      url,
      params,
      headers,
      data
    };

    if (requestTransformer) {
      config.transformRequest = [requestTransformer];
    }

    if (responseTransformer) {
      config.transformResponse = [responseTransformer];
    }

    return axios(config).then((response) => {
      return response.data;
    });
  }
}

const httpService = new Http();

export default httpService;

import cloneDeep from 'lodash/cloneDeep';
import find from 'lodash/find';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

function getModifiedCartBasedOnPromotionRule(promotionRule, currentState) {
  let modifiedState = cloneDeep(currentState);
  if (promotionRule.applyOn === 'unitPrice') {
    let relevantProductAndCount = _getRelevantProductAndCount(
      modifiedState,
      promotionRule
    );

    if (isEmpty(relevantProductAndCount)) {
      throw new Error('This promo code is applicable only when you buy specific products');
    }

    if (_validateCondition(modifiedState, promotionRule)) {
      let product = get((_getAffectingProductAndCount(
        modifiedState,
        promotionRule
      ) || relevantProductAndCount), 'type');
      product.price = _calculateUnitPriceBasedOnUnitPriceReduction(
        product.price,
        promotionRule.unitPriceReduction
      );
      modifiedState.totalPayableAmount = calculateTotalPayableAmount(
        modifiedState.productCountMap
      );
    }
  } else {
    if (_validateCondition(modifiedState, promotionRule)) {
      modifiedState.totalPayableAmount = _calculateTotalPayableAmountBasedOnFlatReduction(
        modifiedState.totalPayableAmount,
        promotionRule.flatReduction
      );
    }
  }

  return modifiedState;
}

function _getRelevantProductAndCount(cart, promotionRule) {
  let applicableOnProduct = promotionRule.relevantFor;
  return find(
    Object.values(cart.productCountMap),
    productAndCount =>
      productAndCount.type._id === applicableOnProduct && productAndCount.count > 0
  );
}

function _getAffectingProductAndCount(cart, promotionRule) {
  let applicableOnProduct = promotionRule.affectOn;
  return find(
    Object.values(cart.productCountMap),
    productAndCount =>
      productAndCount.type._id === applicableOnProduct && productAndCount.count > 0
  );
}

function _validateCondition(cart, promotionRule) {
  let condition = promotionRule.conditions[0];
  let conditionStatement = '';
  let isValid = false;

  switch (condition.conditionType) {
    case 'itemCount': {
      let relevantProductAndCardCount = _getRelevantProductAndCount(
        cart,
        promotionRule
      );

      conditionStatement = `${relevantProductAndCardCount.count} ${condition.clause} ${condition.conditionAmount}`;
      isValid = _safeEval(conditionStatement);
      if (!isValid) {
        throw new Error(
          `Need to buy ${relevantProductAndCardCount.type.productName} ${condition.clause} ${condition.conditionAmount}`
        );
      }
      break;
    }
    case 'priceAmount': {
      conditionStatement = `${cart.totalPayableAmount} ${condition.clause} ${condition.conditionAmount}`;
      isValid = _safeEval(conditionStatement);
      if (!isValid) {
        throw new Error(
          `Total amount needs to be ${cart.totalPayableAmount} ${condition.clause} ${condition.conditionAmount}`
        );
      }
      break;
    }
  }

  return isValid;
}

/**
 * `eval` can be harmful, so creating a safe version of it
 * @param fn
 * @returns {*}
 * @private
 */
function _safeEval(fn) {
  return new Function('return ' + fn)();
}

function calculateTotalPayableAmount(productCountMap) {
  let totalPayableAmount = 0;
  Object.values(productCountMap)
    .forEach((productAndCount) => {
      totalPayableAmount += productAndCount.type.price * productAndCount.count;
    });

  return totalPayableAmount;
}

function _calculateUnitPriceBasedOnUnitPriceReduction(productPrice, unitPriceReductionRule) {
  if (unitPriceReductionRule.by === 'currency') {
    return productPrice - unitPriceReductionRule.amount;
  } else {
    return productPrice - productPrice * (unitPriceReductionRule.amount / 100.0);
  }
}

function _calculateTotalPayableAmountBasedOnFlatReduction(totalAmount, flatReductionRule) {
  if (flatReductionRule.by === 'currency') {
    return totalAmount - flatReductionRule.amount;
  } else {
    return totalAmount - totalAmount * (flatReductionRule.amount / 100.0);
  }
}

export {
  getModifiedCartBasedOnPromotionRule,
  calculateTotalPayableAmount
};
